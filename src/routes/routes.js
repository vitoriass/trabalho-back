const { Router } = require('express');
const ClientController = require('../controllers/ClientController');
const OrderController = require('../controllers/OrderController');
const ProductController = require('../controllers/ProductController');
const StoreController = require('../controllers/StoreController');
const router = Router();

//Client routers
router.get('/Client', ClientController.index);
router.get('/Client/:id', ClientController.show);
router.post('/Client', ClientController.create);
router.put('/Client/:id', ClientController.update);
router.delete('/Client/:id', ClientController.destroy);

//Order routers
router.get('/Order',OrderController.index);
router.get('/Order/:id',OrderController.show);
router.post('/Order', OrderController.create);
router.put('/Order/:id', OrderController.update);
router.delete('/Order/:id', OrderController.destroy);

//Product routers
router.get('/Product',ProductController.index);
router.get('/Product/:id',ProductController.show);
router.post('/Product',ProductController.create);
router.put('/Product/:id', ProductController.update);
router.delete('/Product/:id', ProductController.destroy);

//Store routers
router.get('/Store',StoreController.index);
router.get('/Store/:id',StoreController.show);
router.post('/Store',StoreController.create);
router.put('/Store/:id', StoreController.update);
router.delete('/Store/:id', StoreController.destroy);

module.exports = router;