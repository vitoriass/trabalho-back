const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Order = sequelize.define('Order', {

    total_price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    date_order: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    id_store: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    id_client: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    id_products: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
});

Order.associate = function(models) {
    Order.belongsTo(models.Client);
    Order.hasMany(models.Product);
};

module.exports = Order;