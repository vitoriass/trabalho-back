const { response } = require('express');
const Client = require('../models/Client');

const create = async(req,res) => {
    try{
          const client = await Client.create(req.body);
          return res.status(201).json({message: "Cadastro realizado com sucesso!", client: client});
      }catch(err){
          res.status(500).json({error: err});
      }
};
const index = async(req,res) => {
    try {
        const clients = await Client.findAll();
        return res.status(200).json({clients});
    }catch(err){
        return res.status(500).json({err});
    }
};
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const client = await Client.findByPk(id);
        return res.status(200).json({client});
    }catch(err){
        return res.status(500).json({err});
    }
};
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Client.update(req.body, {where: {id: id}});
        if(updated) {
            const client = await Client.findByPk(id);
            return res.status(200).send(client);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Client.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

// relações incompletas

// const addRelationRole = async(req,res) => {
//     const {id} = req.params;
//     try {
//         const client = await Client.findByPk(id);
//         const role = await Role.findByPk(req.body.RoleId);
//         await client.setRole(role);
//         return res.status(200).json(client);
//     }catch(err){
//         return res.status(500).json({err});
//     }
// };

// const removeRelationRole = async(req,res) => {
//     const {id} = req.params;
//     try {
//         const client = await Client.findByPk(id);
//         await client.setRole(null);
//         return res.status(200).json(client);
//     }catch(err){
//         return res.status(500).json({err});
//     }
// }

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};