const { response } = require('express');
const Order = require('../models/Order');

const create = async(req,res) => {
    try{
          const order = await Order.create(req.body);
          console.log(req.body);
          return res.status(201).json({message: "Pedido criado com sucesso!", order: order});
      }catch(err){
          res.status(500).json({error: err});
      }
};
const index = async(req,res) => {
    try {
        const orders = await Order.findAll();
        return res.status(200).json({orders: orders});
    }catch(err){
        return res.status(500).json({err});
    }
};
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const order = await Order.findByPk(id);
        return res.status(200).json({order});
    }catch(err){
        return res.status(500).json({err});
    }
};
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Order.update(req.body, {where: {id: id}});
        if(updated) {
            const order = await Order.findByPk(id);
            return res.status(200).send(order);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Pedido não encontrado");
    }
};
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Order.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Pedido cancelado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Pedido não encontrado.");
    }
};


module.exports = {
    index,
    show,
    create,
    update,
    destroy
};